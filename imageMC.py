def scatter_plot(x,val_add,axis,color,nome):
 """rappresenta un vettore come un scatter plot, pallini su un grafico"""

 import matplotlib
 import matplotlib.pyplot as plt
 fig = plt.figure()
 X = range(0,x.shape[0])
 Y = x
 plt.ylim([min(Y),min(Y)+val_add])
 plt.axis(axis)
 plt.scatter(X,Y,color=color)
 #fig.show()
 fig.savefig(nome)
 plt.close("all")
 plt.clf()   

def img_BN(path,raggio,nome,formato):
 import os
 from skimage import io
 image_path=path
 all_images = []
 for i in range(0,raggio):
    img = io.imread(image_path+nome+str(i)+formato , as_grey=True)
    all_images.append(img)
    print(i)
 return all_images
 





    

