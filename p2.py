
# coding: utf-8

# In[218]:


import cleaner as cl              #libreria creata per eseguire operazioni su dati

import operazioni as op           #libreria creata con alcune operazioni

import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt


# In[220]:


x=cl.csv_to_matrix('2000.csv',';') #lettura dataset e lo si trasforma in una matrice 
x=cl.reshape(x,1)                  #si converte la matrice (n.m) in una matrice (n*m,1)

k=['2001.csv','2002.csv','2003.csv','2004.csv','2005.csv','2006.csv','2007.csv']
for i in k:
    y=cl.csv_to_matrix(i,';')
    y=cl.reshape(y,1)
    x=cl.append_matrix(x,y)       #si uniscono le matrici per colonna

x.shape


del k, y


# In[221]:


len(x)


# In[222]:



import numpy as np
x=x[x>0]




o=x



# In[224]:


o=o[60:]
y=cl.reshape(o,48)             #dividiamo la matrice (n,1) in una matrice (n/35, 35)


# In[225]:


y.shape


# In[226]:


y


# In[227]:


x=y[:,0:35]
y1=y[:,36:41]
y2=y[:,42:48]


ultimo=x[:,x.shape[1]-1]
ultimo


y3=np.zeros(x.shape[0])

j=0
for i in range(0,len(y2)):
    if y2[i].mean()>ultimo[i]:
        y3[i]=+1
    elif y2[i].mean()<ultimo[i]:
        y3[i]=-1
        
    else:
        y3[i]=0
        

        

x=np.log(x)
x=np.diff(x)


#---------------------------OGGETTO PER LE TIME SERIES----------------------------


from rpy2.robjects import *
from rpy2.robjects import pandas2ri
import rpy2.robjects.numpy2ri
import json
import matplotlib.pyplot as plt
import numpy as np

pandas2ri.activate()

rpy2.robjects.numpy2ri.activate()

class Time_Series():
    
    """ This class calculates changepoints for variance or mean.
 
    Args:
    -----
        methods (list or String) >>> Set a list or a string of all methods.

    """    
    

    
    def __init__(self,methods):
        
                                                 #methods available in this class
        methods_av=['AMOC','BinSeg','SegNeigh','PELT','PROPHET','wbs','ecp','bcp'] 
        self.methods_av=methods_av                   
        
        if type(methods)!= list:                 #convert string to list of methods
            self.methods=[methods]
        else:
            self.methods=methods
            
            
            
        for method in self.methods:              #exception method isn't avalaible
            
            if method not in self.methods_av:     
                stringa=""
                for i in methods_av: 
                    stringa=stringa+i+", "
                    
                raise Exception("Il modello "+method+" non  è presente. Puoi scegliere tra "+stringa[:-1])        
        
        
    def get_libraries(self):
        list_methods = ['bcp', 'wbs', 'prophet', 'ecp', 'xts', 'changepoint', 'RJSONIO', 'hash']
        methods_necessary = []

        for name in list_methods:
            try:
                r('library({})'.format(name))
                print("The package '{}' is already installed.\n".format(name))
            except:
                methods_necessary.append(name)

        print("Missing Packages: ", methods_necessary)

        if len(methods_necessary)>0:
            decision = input("Do you want to continue with the installation of the missing packages? y/n: \n")

            if decision == 'y':
                for package in methods_necessary:
                    print(package)
                    try:
                        r("""install.packages("{}")
                            library({}) 
                            """.format(package, package))
                        print("The package '{}' has been successfully installed!".format(package))
                    except:
                        print("The package '{}' doesn't exist...".format(package))
            else:
                print("The installation has been interrupted...")

        else:
            print("No packages nedeed")
    
    
    
    def get_InfoLibrary(self,library):
         
         r('print(help('+library+'))')     #print the help of a library
        

        
    
    def CP_mean(self,df,penalty_bs="MBIC",penalty_wbs='MBIC',field='y',field_dt='ds',format_dt="%Y-%m-%d",
                     n_checkpoints=5,test_stat="Normal",minseqlen=1,n_intervals=5000, C_wbs = 1.3,
                     sig_lvl=0.05,R=199,min_size=30,alpha=1,p0=0.2,
                     threshold=0.99,growth='linear',changepoint_priorscale=0.05):
        """
        Args:
        -----
            df (pandas.DataFrame) >>> dataframe that contains at least a column with of dates and at least
            another numeric column.
            
            field (string) >>> name of the field (column) that contain numeric values.
            
            field_dt (string) >>> name of the field (column) that contain dates.
            
            format_dt (string) >>> date's format of the "field_dt" column
                        
                                    
            (!) For the methods "BinSeg", "PELT", "AMONG" and "SegNeigh":               

            -n_checkpoints (int) >>> maximum number of checkpoints. (no for AMONG and PELT)
           
            -penalty ("SIC","BIC","MBIC","AIC","Hannan-Quinn","CROPS")>>> kind of penalty.
            
            -test_stat ("Normal" or "CUSUM") The assumed test statistic / distribution of the data.
               
            -minseqlen (int) >>> Positive integer giving the minimum segment length 
                                (no. of observations between changes). 
                                
                                
            (2) For the method "wbs":
            
            -n_checkpoints (int) >>> maximum number of checkpoints.

                                   
            -n_intervals (int) >>> Positive integer giving the numbers of random intervals in each step where
                                will look for checkpoints.
                                   
                                   
            -penalty_wbs ("BIC", "MBIC", "SSIC")>>>kind of penalty.
            
            -C_wbs (Double) the costant for the threshold
            
            
            (3) For the method "ecp":
            
            -n_checkpoints (int) >>> maximum number of checkpoints.

            -sig_lvl (double)>>The level at which to sequentially test if a proposed change point is statistically
                     significant. 
                                 
                                 
            -min.size (double)>>> Minimum number of observations between change points. 
            
            R (int)>>> The maximum number of random permutations to use in each iteration of the
                    permutation test. The permutation test p-value is calculated using the method
                    outlined in Gandy (2009).

            -alpha (int)>>> Alpha The moment index used for determining the distance between and within segments

           (4) For the method "PROPHET":
           
            -n_checkpoints (int) >>> maximum number of checkpoints.

            -growth  ("linear" or "logistic") to specify a linear or logistic trend.


            -changepoint_priorscale (double)>>>Parameter modulating the flexibility of the
            automatic changepoint selection. Large values will allow many changepoints, 
            small values will allow few changepoints.
            
            
           (5) For the method "bcp":
           
           
           -p0 (double)>>>the prior on change point probabilities, U(0, p0), on the probability of a change point 
            at each location in the sequence; for data on a graph, it is the parameter in the partition prior, 
            p0^{l(ρ)}, where l(ρ) is the boundary length of the partition.

            -threshold (double)>>>take the changepoint with a probability > of threshold
           


        Returns:
        --------
            dict with keys the methods and with values the changepoints location.
        """
        
        r.assign('format.dt',format_dt)              #important variables
        r.assign('models',self.methods)                
        df = df.rename(columns={field: 'y', field_dt: 'ds'})
        r.assign('df',df)
        
        r.assign('M',n_intervals)                     #wbs'parameters
        r.assign('penalty_wbs',penalty_wbs.lower()+'.penalty')
        r.assign('th.const',C_wbs)
                  

        r.assign('penalty',penalty_bs)                 #changepoint's parameters
        r.assign('n.checkpoints',n_checkpoints)
        r.assign('test.stat',test_stat)
        r.assign('minseqlen',minseqlen)
        
        r.assign('sig.lvl',sig_lvl)                   #ecp's parameters
        r.assign('R',R)
        r.assign('alpha',alpha)
        r.assign('min.size',min_size)
        
        r.assign('p0',p0)                            #bcp's parameters
        r.assign('threshold',threshold)

        r.assign('growth',growth)                   #prophet's parameters
        r.assign('changepoint.prior.scale',changepoint_priorscale)
        
                
            
            

                       #create a file json with R
        file_json=r("""
        
        
        library(RJSONIO)           #necessary to create json file
                                   #hash as dictionary,where for each method (key) there will be the respective
                                   #value
        library(hash)
        
        h <- hash() 

                                     #we need to convert df to xts and after to ts
        library(xts)        
        
        
        value.xts<- xts(df$y, order.by=as.Date(df$ds, format.dt))
        value.ts<- as.ts(value.xts)
        
        
        models=unlist(models)                          #convert list to array
       
        for (method in models){                        #start for #1
        
        
        #---------------changepoint library........................................
           
          changepoint.models=c('PELT','AMOC','BinSeg','SegNeigh')
          
          if(method %in% changepoint.models){                           #start if #1
             library(changepoint)
             mvalue = cpt.mean(value.ts, method=method,penalty=penalty,Q=n.checkpoints,
             test.stat=test.stat,minseglen=minseqlen)
             loc_cpt=cpts(mvalue)
          
          
          h[[method]] <- loc_cpt

        
                   }                                        #end if #1
                   
                   
        #---------------prophet library------------------------------------------------------------------
        
                   
           else if (method=='PROPHET'){                        #start if #2
              library("prophet")

              m = prophet(df, n.changepoints = n.checkpoints,
              changepoint.prior.scale = changepoint.prior.scale,growth=growth)
                                                             
              cpt.loc<- as.Date(m$changepoints, format.dt)   #checkpoint's location have to be index not date,so
              date=as.Date(df$ds)                             #it will be converted
              loc_cpt<- rep_len(1, length(cpt.loc))           
              i=1
              for(loc in cpt.loc){                        #start for #2
                 loc_cpt[i]=which(date==loc)
                 i=i+1
                      }                                 #end for #2
                      
            h[[method]] <- loc_cpt
        
        
              }                                             #end if #2
              
              
              
        #------------wbs library-----------------------------------------------------------
              
          else if (method=='wbs'){                            #start if #3
            library("wbs")
            w = wbs(df$y,M=M)
            w.cpt = changepoints(w, Kmax = n.checkpoints, penalty = penalty_wbs,th.const=th.const)
            loc_w = w.cpt$cpt.ic[[penalty_wbs]]
            loc_cpt=sort(loc_w)
          
          h[[method]] <- loc_cpt

        }                                                   #end if #3
        
        
        
        #------------ecp library-----------------------------------------------------------------------
        
        
          else if (method=='ecp'){                            #start if #4
            library("ecp")

            e = e.divisive(df, k = n.checkpoints,sig.lvl=sig.lvl,R=R,alpha=alpha,min.size=min.size)
            loc_cpt = e$estimates[2:(length(e$estimates)-1)]
          
          h[[method]] <- loc_cpt

        }                                                   #end if #4
        
        
        
        #------------bcp library-------------------------------------------------------------
        
        
          else if (method=='bcp'){                            #start if #5
        
            library("bcp")

            b = bcp(df$y, p0=p0)
          
            cond = b[["posterior.prob"]]>threshold           #take just the checkpoints with
                                                           #a prob > thereshold
            loc_cpt = which(cond)
          
          h[[method]] <- loc_cpt


        }                                                   #end if #5


        }                                                   #end for #1
        
        
        
        
      

        exportJson <- toJSON(h)                      #creiamo un json con l'hash creata
        

        """)
        
        
                                                         #R'json on python is a array, so it will be
                                                         #convert firstly to string and after to dictionary
                
        s=file_json[0].replace('\n','').replace('   ','')
        json_acceptable_string = s.replace("'", "\"")
        d = json.loads(json_acceptable_string)

        

    
        return d
    
    
    
    
    
    
    
#---------------------------OGGETTO PER LE TIME SERIES----------------------------


from rpy2.robjects import *
from rpy2.robjects import pandas2ri
import rpy2.robjects.numpy2ri
import json
import matplotlib.pyplot as plt
import numpy as np

pandas2ri.activate()

rpy2.robjects.numpy2ri.activate()

class Time_Series():
    
    """ This class calculates changepoints for variance or mean.
 
    Args:
    -----
        methods (list or String) >>> Set a list or a string of all methods.

    """    
    

    
    def __init__(self,methods):
        
                                                 #methods available in this class
        methods_av=['AMOC','BinSeg','SegNeigh','PELT','PROPHET','wbs','ecp','bcp'] 
        self.methods_av=methods_av                   
        
        if type(methods)!= list:                 #convert string to list of methods
            self.methods=[methods]
        else:
            self.methods=methods
            
            
            
        for method in self.methods:              #exception method isn't avalaible
            
            if method not in self.methods_av:     
                stringa=""
                for i in methods_av: 
                    stringa=stringa+i+", "
                    
                raise Exception("Il modello "+method+" non  è presente. Puoi scegliere tra "+stringa[:-1])        
        
        
    def get_libraries(self):
        list_methods = ['bcp', 'wbs', 'prophet', 'ecp', 'xts', 'changepoint', 'RJSONIO', 'hash']
        methods_necessary = []

        for name in list_methods:
            try:
                r('library({})'.format(name))
                print("The package '{}' is already installed.\n".format(name))
            except:
                methods_necessary.append(name)

        print("Missing Packages: ", methods_necessary)

        if len(methods_necessary)>0:
            decision = input("Do you want to continue with the installation of the missing packages? y/n: \n")

            if decision == 'y':
                for package in methods_necessary:
                    print(package)
                    try:
                        r("""install.packages("{}")
                            library({}) 
                            """.format(package, package))
                        print("The package '{}' has been successfully installed!".format(package))
                    except:
                        print("The package '{}' doesn't exist...".format(package))
            else:
                print("The installation has been interrupted...")

        else:
            print("No packages nedeed")
    
    
    
    def get_InfoLibrary(self,library):
         
         r('print(help('+library+'))')     #print the help of a library
        

        
    
    def CP_mean(self,df,penalty_bs="MBIC",penalty_wbs='MBIC',field='y',field_dt='ds',format_dt="%Y-%m-%d",
                     n_checkpoints=5,test_stat="Normal",minseqlen=1,n_intervals=5000, C_wbs = 1.3,
                     sig_lvl=0.05,R=199,min_size=30,alpha=1,p0=0.2,
                     threshold=0.99,growth='linear',changepoint_priorscale=0.05):
        """
        Args:
        -----
            df (pandas.DataFrame) >>> dataframe that contains at least a column with of dates and at least
            another numeric column.
            
            field (string) >>> name of the field (column) that contain numeric values.
            
            field_dt (string) >>> name of the field (column) that contain dates.
            
            format_dt (string) >>> date's format of the "field_dt" column
                        
                                    
            (!) For the methods "BinSeg", "PELT", "AMONG" and "SegNeigh":               

            -n_checkpoints (int) >>> maximum number of checkpoints. (no for AMONG and PELT)
           
            -penalty ("SIC","BIC","MBIC","AIC","Hannan-Quinn","CROPS")>>> kind of penalty.
            
            -test_stat ("Normal" or "CUSUM") The assumed test statistic / distribution of the data.
               
            -minseqlen (int) >>> Positive integer giving the minimum segment length 
                                (no. of observations between changes). 
                                
                                
            (2) For the method "wbs":
            
            -n_checkpoints (int) >>> maximum number of checkpoints.

                                   
            -n_intervals (int) >>> Positive integer giving the numbers of random intervals in each step where
                                will look for checkpoints.
                                   
                                   
            -penalty_wbs ("BIC", "MBIC", "SSIC")>>>kind of penalty.
            
            -C_wbs (Double) the costant for the threshold
            
            
            (3) For the method "ecp":
            
            -n_checkpoints (int) >>> maximum number of checkpoints.

            -sig_lvl (double)>>The level at which to sequentially test if a proposed change point is statistically
                     significant. 
                                 
                                 
            -min.size (double)>>> Minimum number of observations between change points. 
            
            R (int)>>> The maximum number of random permutations to use in each iteration of the
                    permutation test. The permutation test p-value is calculated using the method
                    outlined in Gandy (2009).

            -alpha (int)>>> Alpha The moment index used for determining the distance between and within segments

           (4) For the method "PROPHET":
           
            -n_checkpoints (int) >>> maximum number of checkpoints.

            -growth  ("linear" or "logistic") to specify a linear or logistic trend.


            -changepoint_priorscale (double)>>>Parameter modulating the flexibility of the
            automatic changepoint selection. Large values will allow many changepoints, 
            small values will allow few changepoints.
            
            
           (5) For the method "bcp":
           
           
           -p0 (double)>>>the prior on change point probabilities, U(0, p0), on the probability of a change point 
            at each location in the sequence; for data on a graph, it is the parameter in the partition prior, 
            p0^{l(ρ)}, where l(ρ) is the boundary length of the partition.

            -threshold (double)>>>take the changepoint with a probability > of threshold
           


        Returns:
        --------
            dict with keys the methods and with values the changepoints location.
        """
        
        r.assign('format.dt',format_dt)              #important variables
        r.assign('models',self.methods)                
        df = df.rename(columns={field: 'y', field_dt: 'ds'})
        r.assign('df',df)
        
        r.assign('M',n_intervals)                     #wbs'parameters
        r.assign('penalty_wbs',penalty_wbs.lower()+'.penalty')
        r.assign('th.const',C_wbs)
                  

        r.assign('penalty',penalty_bs)                 #changepoint's parameters
        r.assign('n.checkpoints',n_checkpoints)
        r.assign('test.stat',test_stat)
        r.assign('minseqlen',minseqlen)
        
        r.assign('sig.lvl',sig_lvl)                   #ecp's parameters
        r.assign('R',R)
        r.assign('alpha',alpha)
        r.assign('min.size',min_size)
        
        r.assign('p0',p0)                            #bcp's parameters
        r.assign('threshold',threshold)

        r.assign('growth',growth)                   #prophet's parameters
        r.assign('changepoint.prior.scale',changepoint_priorscale)
        
                
            
            

                       #create a file json with R
        file_json=r("""
        
        
        library(RJSONIO)           #necessary to create json file
                                   #hash as dictionary,where for each method (key) there will be the respective
                                   #value
        library(hash)
        
        h <- hash() 

                                     #we need to convert df to xts and after to ts
        library(xts)        
        
        
        value.xts<- xts(df$y, order.by=as.Date(df$ds, format.dt))
        value.ts<- as.ts(value.xts)
        
        
        models=unlist(models)                          #convert list to array
       
        for (method in models){                        #start for #1
        
        
        #---------------changepoint library........................................
           
          changepoint.models=c('PELT','AMOC','BinSeg','SegNeigh')
          
          if(method %in% changepoint.models){                           #start if #1
             library(changepoint)
             mvalue = cpt.mean(value.ts, method=method,penalty=penalty,Q=n.checkpoints,
             test.stat=test.stat,minseglen=minseqlen)
             loc_cpt=cpts(mvalue)
          
          
          h[[method]] <- loc_cpt

        
                   }                                        #end if #1
                   
                   
        #---------------prophet library------------------------------------------------------------------
        
                   
           else if (method=='PROPHET'){                        #start if #2
              library("prophet")

              m = prophet(df, n.changepoints = n.checkpoints,
              changepoint.prior.scale = changepoint.prior.scale,growth=growth)
                                                             
              cpt.loc<- as.Date(m$changepoints, format.dt)   #checkpoint's location have to be index not date,so
              date=as.Date(df$ds)                             #it will be converted
              loc_cpt<- rep_len(1, length(cpt.loc))           
              i=1
              for(loc in cpt.loc){                        #start for #2
                 loc_cpt[i]=which(date==loc)
                 i=i+1
                      }                                 #end for #2
                      
            h[[method]] <- loc_cpt
        
        
              }                                             #end if #2
              
              
              
        #------------wbs library-----------------------------------------------------------
              
          else if (method=='wbs'){                            #start if #3
            library("wbs")
            w = wbs(df$y,M=M)
            w.cpt = changepoints(w, Kmax = n.checkpoints, penalty = penalty_wbs,th.const=th.const)
            loc_w = w.cpt$cpt.ic[[penalty_wbs]]
            loc_cpt=sort(loc_w)
          
          h[[method]] <- loc_cpt

        }                                                   #end if #3
        
        
        
        #------------ecp library-----------------------------------------------------------------------
        
        
          else if (method=='ecp'){                            #start if #4
            library("ecp")

            e = e.divisive(df, k = n.checkpoints,sig.lvl=sig.lvl,R=R,alpha=alpha,min.size=min.size)
            loc_cpt = e$estimates[2:(length(e$estimates)-1)]
          
          h[[method]] <- loc_cpt

        }                                                   #end if #4
        
        
        
        #------------bcp library-------------------------------------------------------------
        
        
          else if (method=='bcp'){                            #start if #5
        
            library("bcp")

            b = bcp(df$y, p0=p0)
          
            cond = b[["posterior.prob"]]>threshold           #take just the checkpoints with
                                                           #a prob > thereshold
            loc_cpt = which(cond)
          
          h[[method]] <- loc_cpt


        }                                                   #end if #5


        }                                                   #end for #1
        
        
        
        
      

        exportJson <- toJSON(h)                      #creiamo un json con l'hash creata
        

        """)
        
        
                                                         #R'json on python is a array, so it will be
                                                         #convert firstly to string and after to dictionary
                
        s=file_json[0].replace('\n','').replace('   ','')
        json_acceptable_string = s.replace("'", "\"")
        d = json.loads(json_acceptable_string)

        

    
        return d
    
    
    
    
    
    
    
    def CP_var(self,df,penalty="MBIC",field='y',field_dt='ds',format_dt="%Y-%m-%d",
                     n_checkpoints=5,test_stat="Normal",minseqlen=1):
        """
        Args:
        -----
            df (pandas.DataFrame) >>> dataframe that contains at least a column with of dates and at least
            another numeric column.
            
            field (string) >>> name of the field (column) that contain numeric values.
            
            field_dt (string) >>> name of the field (column) that contain dates.
            
            format_dt (string) >>> date's format of the "field_dt" column
            
            n_checkpoints (int) >>> maximum number of checkpoints. (no for AMONG and PELT)
            
           
            -penalty ("SIC","BIC","MBIC","AIC","Hannan-Quinn","CROPS")>>> kind of penalty.
            
            -test_stat ("Normal" or "CUSUM") The assumed test statistic / distribution of the data.
               
            -minseqlen (int) >>> Positive integer giving the minimum segment length 
                                (no. of observations between changes). 
            
                                


        Returns:
        --------
            dict with keys the methods and with values the changepoints location.
        """
                
         
            
        for method in self.methods:                 #check if method are avalaible for the variance checkpoints
            if (method not in ['PELT','AMOC','BinSeg','SegNeigh']):
                print("Can't use "+method+" to look for CP in variance ")
                
                
        r.assign('models',self.methods)            #changepoint's parameters
        r.assign('penalty',penalty)
        r.assign('n.checkpoints',n_checkpoints)
        r.assign('test.stat',test_stat)
        r.assign('minseqlen',minseqlen)
        
        
        
        

            
        df = df.rename(columns={var_valori: 'y', var_date: 'ds'}) #important variables
        r.assign('df',df) 
        r.assign('format.dt',format_dt)
            
            



        

        file_json=r("""
        
        
        library(RJSONIO)           

        library(hash)
        
        h <- hash() 

        library(xts)        
        
        
        value.xts<- xts(df$y, order.by=as.Date(df$ds, format.dt))
        value.ts<- as.ts(value.xts)
        
        
        models=unlist(models)                         
       
        for (method in models){                        #start for #1
        
        
        #---------------changepoint library.................................................................
           
          changepoint.models=c('PELT','AMOC','BinSeg','SegNeigh')
          
          if(method %in% changepoint.models){                           #start if #1
          library(changepoint)
          mvalue = cpt.var(value.ts, method=method,penalty=penalty,Q=n.checkpoints-1,
          test.stat=test.stat,minseglen=minseqlen)
          loc_cpt=cpts(mvalue)
          
          
          h[[method]] <- loc_cpt

        
                   }                                        #end if #1
                   

                   


        }                                                   #end for #1
        
        
         exportJson <- toJSON(h)                      
        

        """)
        

                
        s=file_json[0].replace('\n','').replace('   ','')
        json_acceptable_string = s.replace("'", "\"")
        d = json.loads(json_acceptable_string)

        

    
        return d
    
    
    
    def CP_plot(self,df,x,field='y',kind='V'):
        """
        Args:
        -----
            df (pandas.DataFrame) >>> dataframe that contains at least a column numeric column.

            field (string) >>> name of the field (column) that contain numeric values.

            x (list) >>> list with the locations of checkpoint

            kind ("V" or "H") >>> kind of plot, if you want with vertical line or horizzontal line
                                  on each checkpoint's location.




        Returns:
        --------
            Nothing
        """





            
        if (kind not in ['V','H','P','N']):                    #check on kind of graphics
            raise Exception('Kind of graphics unavailable- You can chooice kind="V" or kind="H"')


        if kind=='V':                                   #vertical CP


            s=df[field]

            CP=np.asarray(x)

            plt.plot(s.values,label='Serie')

            
            cp0=x[0]

            cp1=x[1]

            h0=df[field][0:cp0].mean()
            h1=df[field][cp0:cp1].mean()

            if (h1>h0):
                plt.axvline(cp0, color='red', linestyle='--',label='CP')
            else:
                plt.axvline(cp0, color='blue', linestyle='--',label='CP')

            for i in range(1,len(CP)):

                cp0=x[i-1]


                cp1=x[i]
                try:            
                    cp2=x[i+1]
                except:
                    cp2=len(df[field])


                h0=df[field][cp0:cp1].mean()
                h1=df[field][cp1:cp2].mean()

                if (h1>h0):
                    plt.axvline(CP[i], color='red', linestyle='--')
                else:
                    plt.axvline(CP[i], color='blue', linestyle='--')            


            plt.grid(True)
            plt.legend(frameon=False)        

            plt.title("Changepoints")


        elif(kind=='H'):                              #horizzontal CP

            s=df[field]

            plt.plot(s,label='Serie')
            CP=np.asarray(x)
            lung=CP[0]-0
            maxim=s[0:lung].mean()
            m=np.repeat(maxim, lung)

            plt.plot(range(0,lung),m, color='red',linewidth=3,label='CP')

            for i in range(1,len(CP)):
                lung=CP[i]-CP[i-1]
                lung
                maxim=s[CP[i-1]:CP[i]].mean()
                m=np.repeat(maxim, CP[i]-CP[i-1])
                plt.plot(range(CP[i-1],CP[i]),m, color='red',linewidth=3)


            maxim=s[CP[len(CP)-1]:len(s)].mean()
            m=np.repeat(maxim, len(s)-CP[len(CP)-1])


            plt.title("Changepoints")

            plt.plot(range(CP[len(CP)-1],len(s)),m, color='red',linewidth=3)
            plt.grid(True)
            plt.legend(frameon=False)




        elif kind=='P':                                   #vertical CP


            s=df[field]

            CP=np.asarray(x)

            plt.plot(s.values,label='Serie',color='black')
            
            

                   
            try:
                cp1=x[1]
                cp0=x[0]

            except:
                cp0=x

                cp1=len(df[field])
                

            h0=df[field][0:cp0].mean()
            h1=df[field][cp0:cp1].mean()

            m=s.max()+0.00005*s.max()


            if (h1>h0):

                plt.plot(cp0,m,'ro',color='black') 

            else:
                plt.plot(cp0,m,'ro',color='lightgray') 
                
            try:    

                for i in range(1,len(CP)):

                    cp0=x[i-1]


                    cp1=x[i]
                    try:            
                        cp2=x[i+1]
                    except:
                        cp2=len(df[field])


                    h0=df[field][cp0:cp1].mean()
                    h1=df[field][cp1:cp2].mean()

                    if (h1>h0):
                        plt.plot(CP[i],m,'ro',color='black') 

                    else:
                        plt.plot(CP[i],m,'ro',color='lightgray') 
            except:
                  print('')
                    
                    
        else :                                   #vertical CP


            s=df[field]

            plt.plot(s.values,label='Serie')



            plt.grid(False)
    
    

        
        
    


















   
  


import pandas as pd

TS=Time_Series(methods=['wbs'] )

from datetime import datetime, timedelta

base = datetime(2000, 1, 1)
b = np.array([base + timedelta(days=i) for i in range(x.shape[1])])
for i in range(0,x.shape[1]):
    b[i]=b[i].strftime("%Y-%m-%d")
    




inizio=0


d=10

n=179000
p=0.7


import matplotlib.pyplot as plt


k=inizio+179001
for i in range(inizio,round(n*p)):
    
    #df=pd.DataFrame()
    #a=x[i,:]
    #df = pd.DataFrame({'Date':b, 'y':a})
    #cp=TS.CP_mean(df,n_checkpoints=50,field='y',field_dt='Date',penalty_bs='AIC',penalty_wbs='SSIC',n_intervals=2500)
    #TS.CP_plot(df,x=cp['wbs'],field='y',kind='P')
    plt.plot(x[i,:])
    plt.axis('off')
    
    if(y3[i]==-1):
        
        plt.savefig("dataset/training/down/X"+str(k)+".png",dpi=d)
        k+=1
        
        
    elif(y3[i]==1):
        plt.savefig("dataset/training/up/X"+str(k)+".png",dpi=d)
        k+=1
    else:

        k+=1
         
        



    plt.close("all")
    plt.clf()
    print(i)
    
    
for i in range(round(p*n),n):

    #df=pd.DataFrame()
    #a=x[i,:]
    #df = pd.DataFrame({'Date':b, 'y':a})
    #cp=TS.CP_mean(df,n_checkpoints=50,field='y',field_dt='Date',penalty_bs='AIC',penalty_wbs='SSIC',n_intervals=2500)
    #TS.CP_plot(df,x=cp['wbs'],field='y',kind='P')
    plt.plot(x[i,:])
    plt.axis('off')
    
    if(y3[i]==-1):
        
        plt.savefig("dataset/testing/down/X"+str(k)+".png",dpi=d)
        k+=1



    elif(y3[i]==1):
        plt.savefig("dataset/testing/up/X"+str(k)+".png",dpi=d)
        k+=1
    else:

        k+=1
 
        



    plt.close("all")
    plt.clf() 
    print(i)
    



