def max_col(y):
    """data una matrice da come output il massimo per ogni colonna"""
    massimi=y.max(axis=1)
    return massimi

def min_col(y):
    """data una matrice da come output il massimo per ogni colonna"""
    minimi=y.min(axis=1)
    return minimi
