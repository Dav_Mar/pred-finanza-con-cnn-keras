import cleaner as cl              #libreria creata per eseguire operazioni su dati
import imageMC as imgMC           #libreria creata per trattare le immagini
import operazioni as op           #libreria creata con alcune operazioni
import numpy as np
import pandas as pd


from keras.models import Sequential    #librerie per CNN
from keras.utils import to_categorical
from keras.callbacks import Callback
from keras.layers.core import Dense, Activation
from keras import layers
from keras.optimizers import SGD
from keras.utils import np_utils


from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense

import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt



n=12500
p=0.8


seed = 7

#Initialising the CNN
model = Sequential()



model.add(Conv2D(24, 5, 5, activation = 'relu', input_shape = (60, 40, 1), init='he_normal'))

# For an explanation on pooling layers see http://cs231n.github.io/convolutional-networks/#pool
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(50, 5, 5, activation = 'relu', init='he_normal'))

model.add(MaxPooling2D(pool_size=(2, 2)))

# Flatten the 3D output to 1D tensor for a fully connected layer to accept the input
model.add(Flatten())
model.add(Dense(180, activation = 'relu', init='he_normal'))
model.add(layers.Dropout(0.5))
model.add(Dense(100, activation = 'relu', init='he_normal'))
model.add(layers.Dropout(0.5))
model.add(Dense(3, activation = 'softmax', init='he_normal')) #Last layer with one output per class

# The function to optimize is the cross entropy between the true label and the output (softmax) of the model
# We will use adadelta to do the gradient descent see http://cs231n.github.io/neural-networks-3/#ada
from keras.optimizers import SGD




# Part 2 - Fitting the CNN to the images

from keras.preprocessing.image import ImageDataGenerator

#applichiamo delle traformazioni casuali per aumentare il numero di esempi nel training
train_datagen = ImageDataGenerator(rescale = 1./255)

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('dataset/training',
                                                 target_size = (60, 40), #dimensione in ingresso alla cnn
                                                 color_mode="grayscale",
                                                 batch_size = 32,
                                                 class_mode = 'categorical',
                                                 shuffle=False) #se la classe è binaria o meno 
                                                 
# NB per aumentare ulteriormente le perfomance si potrebbe aumentare la dimensione delle immagini in ingresso,
# tuttavia il tempo necessario all'esecuzione aumenta notevolemente                                                 

#ripetiamo la stessa cosa anche per il test set        
test_set = test_datagen.flow_from_directory('dataset/testing',
                                            target_size = (60, 40),
                                            batch_size = 32,
                                            color_mode="grayscale",
                                           class_mode = 'categorical',
                                           shuffle=False)
#Time-Based Learning Rate Schedule


epochs = 120
learning_rate = 0.01
decay_rate = learning_rate / epochs
momentum = 0.8
sgd = SGD(lr=learning_rate, momentum=momentum, decay=decay_rate, nesterov=False)
model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
# Fit the model

#Drop-Based Learning Rate Schedule

#sgd = SGD(lr=0.0, momentum=0.9, decay=0.0, nesterov=False)
#model.compile(loss='binary_crossentropy', optimizer=sgd, metrics=['accuracy'])
# learning schedule callback
#lrate = LearningRateScheduler(step_decay)
#callbacks_list = [lrate]
# Fit the model
#model.fit(X, Y, validation_split=0.33, epochs=50, batch_size=28, callbacks=callbacks_list, verbose=2)

model.fit_generator(training_set,
                         steps_per_epoch = 35000,   #numero di immagini nel trainig set (tutte le osservazioni vengono passate durante ogni epoca)
                         epochs = epochs,
                         validation_data = test_set,
                         validation_steps = 15000,
                         verbose=1)   #corrisponde al numero di immagini nel mio test set

