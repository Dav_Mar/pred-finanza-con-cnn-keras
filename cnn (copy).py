# Convolutional Neural Network

# Installing Theano
# pip install --upgrade --no-deps git+git://github.com/Theano/Theano.git

# Installing Tensorflow
# pip install tensorflow

# Installing Keras
# pip install --upgrade keras

# Part 1 - Building the CNN

# Importing the Keras libraries and packages
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense

# Initialising the CNN
classifier = Sequential()

# Step 1 - Convolution
#il primo parametro è il numero di filtri che vogliamo utilizzare e quindi il numero di 
#feature map che vogliamo creare. Poi abbiamo la dimensione di ogni maschera. Uno standard
#è impostare come numero di filtri 32 e nel caso poi aggiungere altri strati di convoluzione
#con un numero maggiore di filtri (128, 256).
#Un parametro molto importante è il size dell'ingresso in quanto le immagini non avranno
#tutte la stessa dimensione, quindi dovremo forzare la loro size.  Se usiamo immagine a colori
#avremo dei 3D array (RGB). Quindi i primi due numeri sono i pixel per ogni canale, l'ultimmo 
#il numero di canali (con Theano questo ordine sarebbe invertito nb). 
classifier.add(Conv2D(32, (3, 3), input_shape = (64, 64, 3), activation = 'relu'))

# Step 2 - Pooling
#mettiamo la metà della dimensione della maschera (3+1 / 2). In generale 2x2 è una dimensione
#ottima che conviene utilizzare sempre 
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Adding a second convolutional layer
#questa è la soluzione migliore se si volessero incrementare le perfomance (si potrebbero anche aumentare
# il numero di strati nella rete di uscita fully connect ma questo metodo in generale è migliore) e quindi
#si rende la rete ancora più profonda. Si noti che l'ingresso non è l'immagine ma l'usicta dello strato precedente
#e quindi non mettiamo il parametro input_shape
classifier.add(Conv2D(32, (3, 3), activation = 'relu'))

#Pooling per il secondo strato aggiunto
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Step 3 - Flattening
classifier.add(Flatten())

# Step 4 - Full connection
classifier.add(Dense(units = 128, activation = 'relu'))
classifier.add(Dense(units = 1, activation = 'sigmoid'))

# Compiling the CNN
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# Part 2 - Fitting the CNN to the images

from keras.preprocessing.image import ImageDataGenerator

#applichiamo delle traformazioni casuali per aumentare il numero di esempi nel training
train_datagen = ImageDataGenerator(rescale = 1./255,   #per avere tutti i pixel tra 0-1
                                   shear_range = 0.2,
                                   zoom_range = 0.2,
                                   horizontal_flip = True)

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('dataset/training_set',
                                                 target_size = (64, 64), #dimensione in ingresso alla cnn
                                                 batch_size = 32,       
                                                 class_mode = 'binary') #se la classe è binaria o meno 
                                                 
# NB per aumentare ulteriormente le perfomance si potrebbe aumentare la dimensione delle immagini in ingresso,
# tuttavia il tempo necessario all'esecuzione aumenta notevolemente                                                 

#ripetiamo la stessa cosa anche per il test set        
test_set = test_datagen.flow_from_directory('dataset/test_set',
                                            target_size = (64, 64),
                                            batch_size = 32,
                                            class_mode = 'binary')

classifier.fit_generator(training_set,
                         steps_per_epoch = 8000,   #numero di immagini nel trainig set (tutte le osservazioni vengono passate durante ogni epoca)
                         epochs = 25,
                         validation_data = test_set,
                         validation_steps = 2000)   #corrisponde al numero di immagini nel mio test set